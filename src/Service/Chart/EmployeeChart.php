<?php


namespace App\Service\Chart;


use App\Database\Entity\Employee;

class EmployeeChart
{
    private const AGE_END = 70;
    private const AGE_STEP = 10; // must be larger than 1

    /**
     * @param Employee[] $employees
     */
    public function makeChartData(array $employees): array
    {
        $ageGroups = range(0, self::AGE_END, self::AGE_STEP);
        $finalAge = self::AGE_END + self::AGE_STEP;

        foreach ($ageGroups as $ageStart) {
            $ageEnd = $ageStart + self::AGE_STEP - 1;
            $groupCount["$ageStart-$ageEnd"] = 0;
        }
        $groupCount[$finalAge."+"] = 0;

        foreach ($employees as $employee) {
            $employeeAge = $employee->getAge();
            if (null === $employeeAge) {
                continue;
            }

            foreach ($ageGroups as $ageStart) {
                $ageEnd = $ageStart + 9;
                if ($employeeAge >= $ageStart && $employeeAge <= $ageEnd) {
                    $groupCount["$ageStart-$ageEnd"]++;
                }
            }
            if ($employeeAge >= $finalAge) {
                $groupCount[($finalAge)."+"]++;
            }
        }

        return [
            'keys' => array_keys($groupCount),
            'values' => array_values($groupCount),
        ];
    }
}
