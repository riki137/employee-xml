<?php


namespace App\Controller;


use App\Database\Entity\Employee;
use App\Database\FileDatabaseManager;
use App\Form\EmployeeType;
use App\Service\Chart\EmployeeChart;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\InvalidCsrfTokenException;

class EmployeeController extends AbstractController
{
    /**
     * @Route(path="/", name="employees.list")
     */
    public function index(FileDatabaseManager $fileDatabaseManager): Response
    {
        return $this->render('employees/list.html.twig', [
            'employees' => $fileDatabaseManager->load()->getEmployees(),
        ]);
    }

    /**
     * @param FileDatabaseManager $fileDatabaseManager
     * @param Request             $request
     * @return Response
     * @Route(path="/employees/create", name="employees.create")
     */
    public function create(FileDatabaseManager $fileDatabaseManager, Request $request): Response
    {
        $employee = new Employee();
        $form = $this->createForm(EmployeeType::class, $employee);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $database = $fileDatabaseManager->load();
            $database->persistEmployee($employee);
            $fileDatabaseManager->save($database);

            return $this->redirectToRoute('employees.list', ['saved' => 1]);
        }

        return $this->render('employees/edit.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @param FileDatabaseManager $fileDatabaseManager
     * @param EmployeeChart       $employeeChart
     * @return Response
     * @Route(path="/employees/age-chart", name="employees.age_chart")
     */
    public function ageChart(FileDatabaseManager $fileDatabaseManager, EmployeeChart $employeeChart): Response
    {
        return $this->render('employees/age_chart.html.twig', [
            'chartData' => $employeeChart->makeChartData(
                $fileDatabaseManager->load()->getEmployees()
            ),
        ]);
    }

    /**
     * @param FileDatabaseManager $fileDatabaseManager
     * @param Request             $request
     * @param string              $employeeId
     * @return Response
     * @Route(path="/employees/{employeeId}", name="employees.edit")
     */
    public function edit(FileDatabaseManager $fileDatabaseManager, Request $request, string $employeeId): Response
    {
        $employees = $fileDatabaseManager->load()->getEmployees();
        $employee = $employees[$employeeId] ?? null;
        if (null === $employee) {
            throw new NotFoundHttpException("Employee with ID $employeeId not found");
        }

        $form = $this->createForm(EmployeeType::class, $employee);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $database = $fileDatabaseManager->load();
            $database->persistEmployee($employee);
            $fileDatabaseManager->save($database);

            return $this->redirectToRoute('employees.list', ['saved' => 1]);
        }

        return $this->render('employees/edit.html.twig', [
            'employee' => $employee,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @param FileDatabaseManager $fileDatabaseManager
     * @param string              $employeeId
     * @param string              $token
     * @return RedirectResponse|Response
     * @Route(path="/employees/{employeeId}/delete/{token}", name="employees.delete")
     */
    public function delete(FileDatabaseManager $fileDatabaseManager, string $employeeId, string $token): Response
    {
        if (!$this->isCsrfTokenValid('employeeDelete', $token)) {
            throw new InvalidCsrfTokenException();
        }

        $database = $fileDatabaseManager->load();
        $database->removeEmployee($employeeId);
        $fileDatabaseManager->save($database);

        return $this->redirectToRoute('employees.list', ['deleted' => 1]);
    }
}
