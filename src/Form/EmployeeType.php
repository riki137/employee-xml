<?php


namespace App\Form;


use App\Database\Entity\Gender;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class EmployeeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('firstName', TextType::class)
            ->add('lastName', TextType::class)
            ->add('gender', ChoiceType::class, [
                'choices' => [
                    'employees.gender.female' => Gender::FEMALE,
                    'employees.gender.male' => Gender::MALE,
                    'employees.gender.other' => Gender::OTHER,
                ]
            ])
            ->add('birthday', DateType::class, [
                'years' => range(1900, date('Y')),
            ])
        ;
    }
}
