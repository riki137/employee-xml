<?php


namespace App\Exception;


use Throwable;

class InvalidConstantException extends \RuntimeException
{
    public const CODE = 1001;

    public function __construct(Throwable $previous = null)
    {
        parent::__construct("You specified an invalid constant", self::CODE, $previous);
    }
}
