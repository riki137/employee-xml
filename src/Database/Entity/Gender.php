<?php


namespace App\Database\Entity;


/**
 * Normally i'd make a utility class for enum constants, but since this is a one-off, i'll use very simple logic here.
 */
class Gender
{
    public const MALE = 'male';
    public const FEMALE = 'female';
    public const OTHER = 'other';

    public const CHOICES = [
        self::MALE => self::MALE,
        self::FEMALE => self::FEMALE,
        self::OTHER => self::OTHER,
    ];
}
