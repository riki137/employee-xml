<?php


namespace App\Database\Entity;


use App\Exception\InvalidConstantException;
use Carbon\CarbonImmutable;
use DateTimeImmutable;
use Faker\Factory;
use Symfony\Component\Validator\Constraints as Assert;

class Employee
{
    /**
     * @var string uniqid
     */
    private string $id;

    /**
     * @var string|null
     * @Assert\NotBlank
     */
    private ?string $firstName = null;

    /**
     * @var string|null
     * @Assert\NotBlank
     */
    private ?string $lastName = null;

    /**
     * @var string|null
     * @Assert\Choice(choices=Gender::CHOICES)
     * @Assert\NotBlank
     * @see Gender enum constant used here
     */
    private ?string $gender = null;

    /**
     * @var DateTimeImmutable|null
     * @Assert\LessThanOrEqual("today")
     * @Assert\NotBlank
     *
     * I only use immutable dates.
     */
    private ?DateTimeImmutable $birthday = null;

    public function __construct()
    {
        $this->id = uniqid('ID', true);

        $faker = Factory::create('sk_SK');
        $this->setFirstName($faker->firstName)
            ->setLastName($faker->lastName)
            ->setGender($faker->randomElement(Gender::CHOICES))
            ->setBirthday($faker->dateTimeBetween('-100 years', '-5 years'))
        ;
        // Faker is used for easy data example creation.
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(?string $firstName): Employee
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(?string $lastName): Employee
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getGender(): ?string
    {
        return $this->gender;
    }

    public function setGender(?string $gender): Employee
    {
        if (!array_key_exists($gender, Gender::CHOICES)) {
            // ensure no wrong values get into the class instance
            throw new InvalidConstantException();
        }

        $this->gender = $gender;

        return $this;
    }

    /**
     * Helper methods in entities might not be a good idea in large-scale projects.
     * However i consider this a respectful helper method.
     * @return int
     */
    public function getAge(): ?int
    {
        $birthday = $this->getBirthday();
        if ($birthday === null) {
            // Age should never be null.
            return null;
        }

        return $birthday->diffInYears();
    }

    public function getBirthday(): ?CarbonImmutable
    {
        if (null === $this->birthday) {
            return null;
        }
        // I use Carbon in getter for better datetime manipulation, however i store plain DateTimeImmutable in the entity.
        return CarbonImmutable::instance($this->birthday);
    }

    public function setBirthday(\DateTimeInterface $birthday): Employee
    {
        $this->birthday = CarbonImmutable::instance($birthday)->toDateTimeImmutable();

        return $this;
    }
}
