<?php


namespace App\Database;


use App\Database\Entity\Employee;
use JMS\Serializer\Annotation as Serializer;

/**
 * This is a data class used for serializing into an XML file.
 */
class FileDatabase
{
    /**
     * @var Employee[]
     * @Serializer\XmlMap()
     * @Serializer\Type("array<string, App\Database\Entity\Employee>")
     */
    private array $employees = [];

    /**
     * @return Employee[]
     */
    public function getEmployees(): array
    {
        return $this->employees;
    }

    public function persistEmployee(Employee $employee): void
    {
        $this->employees[$employee->getId()] = $employee;
    }

    public function removeEmployee(string $employeeId): void
    {
        unset($this->employees[$employeeId]);
    }
}
