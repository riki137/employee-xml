<?php


namespace App\Database;


use JMS\Serializer\Serializer;
use JMS\Serializer\SerializerBuilder;

class FileDatabaseManager
{
    private const FORMAT = 'xml';
    private const XML_LOCATION = '/var/FileDatabase.xml';

    private Serializer $serializer;
    private string $dbPath;

    public function __construct()
    {
        // JMS Serializer is used for serialization
        $this->serializer = SerializerBuilder::create()->build();
        $this->dbPath = dirname(__DIR__, 2).self::XML_LOCATION;
    }

    public function save(FileDatabase $fileDatabase): void
    {
        $success = file_put_contents(
            $this->dbPath,
            $this->serializer->serialize($fileDatabase, self::FORMAT)
        );
        if ($success === false) {
            throw new \RuntimeException('FileDatabase is not writable');
        }
    }

    public function load(): FileDatabase
    {
        if (!file_exists($this->dbPath)) {
            // I decided to use a class instance as database for straight-forward un/serialization
            // New database is created if no such file exists.
            return new FileDatabase();
        }
        if (!is_readable($this->dbPath)) {
            throw new \RuntimeException('FileDatabase is not readable, but exists');
        }

        $contents = file_get_contents($this->dbPath);

        return $this->serializer->deserialize(
            $contents,
            FileDatabase::class,
            self::FORMAT
        );
    }
}
